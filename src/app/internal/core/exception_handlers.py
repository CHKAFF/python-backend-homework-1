from functools import partial

from ninja import NinjaAPI

from app.internal.core.exceptions import ForbiddenException, UnauthorizedException


def unauthorized_exception_handler(request, exc, api: NinjaAPI):
    return api.create_response(
        request,
        {"message": f"Unauthorized. {exc.message}"},
        status=401,
    )


def forbidden_exception_handler(request, exc, api: NinjaAPI):
    return api.create_response(
        request,
        {"message": f"Forbidden. {exc.message}"},
        status=403,
    )


def add_exception_handlers(api: NinjaAPI):
    api.add_exception_handler(UnauthorizedException, partial(unauthorized_exception_handler, api=api))
    api.add_exception_handler(ForbiddenException, partial(forbidden_exception_handler, api=api))
    return api
