from telegram import Update
from telegram.ext import CallbackContext

from app.internal.telegram_users.db.models import TelegramUser
from ...services.telegram_user_service import TelegramUserService

def phone_number_should_be_filled(function):
    def wrapper(update: Update, context: CallbackContext):
        external_id = update.message.chat_id
        tg_user_service = TelegramUserService()
        
        user = tg_user_service.get(external_id)
        
        if (user.phone_number == ""):
            message = f'Please set phone number. Use the /set_phone command to save the phone number.'
            context.bot.send_message(chat_id=update.effective_chat.id, text=message)
            return
        
        return function(update, context)

    return wrapper