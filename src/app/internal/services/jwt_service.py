import uuid
import jwt
from datetime import datetime, timedelta
from typing import Any

from ..tokens.db.models import Token
from ..telegram_users.db.models import TelegramUser
from django.conf import settings

ENCODE_ALGORITHM = "HS256"
class JWTService:
    def create_access_token(user_id):
        expires_at = (datetime.utcnow() + timedelta(minutes=10)).timestamp() #Можно вынести в переменную окружения
        access_token = jwt.encode(
            payload={"user_id": user_id, "expires_at": expires_at},
            key=settings.JWT_SECRET,
            algorithm=ENCODE_ALGORITHM,
        )
        return access_token, expires_at

    def create_refresh_token(user):
        refresh_token = Token.objects.create(jti=uuid.uuid4(), user=user)
        return jwt.encode(payload={"jti": str(refresh_token.jti)}, key=settings.JWT_SECRET, algorithm=ENCODE_ALGORITHM)

    def get_refresh_token_by_jti(jti):
        return Token.objects.select_related("user").filter(jti=jti).first()

    def remove_refresh_token_by_person(user):
        deleted, _ = Token.objects.filter(user=user).delete()
        return deleted

    def decode_token(token):
        return jwt.decode(token, key=settings.JWT_SECRET, algorithms=[ENCODE_ALGORITHM])

    def encode_payload(payload: dict[str, Any]):
        return jwt.encode(payload=payload, key=settings.JWT_SECRET, algorithm=ENCODE_ALGORITHM)

