migrate:
	python3 app/src/manage.py migrate $(if $m, api $m,)

makemigrations:
	python3 src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	python3 src/manage.py createsuperuser

collectstatic:
	python3 src/manage.py collectstatic --no-input

dev:
	python3 src/manage.py runserver localhost:8000

command:
	python3 src/manage.py ${c}

shell:
	python3 src/manage.py shell

debug:
	python3 src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

prod:
	python3 src/manage.py runserver localhost:80

run_bot:
	python3 src/manage.py run_bot

dc_up:
	docker-compose up -d

dc_down:
	docker-compose down

dc_build:
	docker-compose build --no-cache

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}

test:
	docker-compose run app python3 -m pytest src/tests

test_smoke:
	docker-compose run app pytest src/tests/smoke

test_integration:
	docker-compose run app pytest src/tests/integration

test_unit:
	docker-compose run app pytest src/tests/unit


