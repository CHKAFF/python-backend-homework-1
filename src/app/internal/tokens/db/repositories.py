import uuid

from .models import Token

class ITokenRepository:
    def create_issued_token(self, telegram_user):
        ...

    def get_issued_token_by_jti(self, jti) :
        ...


class TokenRepository(ITokenRepository):
    def create_issued_token(self, telegram_user):
        return Token.objects.create(jti=uuid.uuid4(), user=telegram_user)

    def get_issued_token_by_jti(self, jti):
        return Token.objects.select_related("user").filter(jti=jti).first()
