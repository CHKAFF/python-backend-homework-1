import json
from django.views import View
from django.http import HttpResponse, JsonResponse
from ...bot import TelegramBot
from app.internal.telegram_users.db.models import TelegramUser
from ...services.telegram_user_service import TelegramUserService
from .decorators import authorize
from django.conf import settings

@authorize
def me(request, id):
    tg_user_services = TelegramUserService()
    
    user = tg_user_services.get(id)
    
    if user == None:
        return JsonResponse({'id': id, 'message': "User in not exist."})
    
    return JsonResponse({'id': user.external_id, 'name': user.name, "phone_number": user.phone_number})

bot = TelegramBot(settings.TELEGRAM_BOT_TOKEN)

class BotView(View):
    def post(self, request, *args, **kwargs):
        t_data = json.loads(request.body)
        bot.process(t_data)
        return JsonResponse({"OK": "POST request processed"})



