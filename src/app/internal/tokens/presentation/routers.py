from ninja import NinjaAPI, Router

from ..domain.entities import JWTTokensSchema
from .handlers import TokenHandlers
from app.internal.core.responses import ForbiddenResponse, UnauthorizedResponse


def get_auth_router(handler: TokenHandlers) -> Router:
    router = Router(tags=["auth"])

    router.add_api_operation(
        "/login/",
        ["POST"],
        handler.login,
        response={200: JWTTokensSchema, 401: UnauthorizedResponse, 403: ForbiddenResponse},
        url_name="auth/login",
        auth=None,
    )

    router.add_api_operation(
        "/refresh/",
        ["POST"],
        handler.refresh,
        response={200: JWTTokensSchema, 401: UnauthorizedResponse},
        url_name="auth/refresh",
        auth=None,
    )

    return router
W

def add_auth_router(api: NinjaAPI, handler: TokenHandlers):
    auth_router = get_auth_router(handler)
    api.add_router("/auth", auth_router)
