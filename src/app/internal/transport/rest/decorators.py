from datetime import datetime
from django.http import HttpRequest, HttpResponse

from ...services.telegram_user_service import TelegramUserService
from ...services.jwt_service import JWTService


def authorize(function):
    def wrapper(request: HttpRequest, id = None):
        try:
            authorization_header = request.headers.get("authorization").split() #Сделать не черз исключение, вручную
            if len(authorization_header) == 2 and authorization_header[0].lower() == "bearer":
                access_token = authorization_header[1]
                payload = JWTService.decode_token(access_token)
                expires_at = payload["expires_at"]
                if expires_at < datetime.utcnow().timestamp():
                    return HttpResponse("401 Unauthorized. Access token expired", status=401)
                tg = TelegramUserService()
                person = tg.get(external_id=payload["user_id"]) # передавать дальше (?)
            else:
                return HttpResponse(
                    "401 Unauthorized. Specify access token. Example: Bearer {access_token}", status=401
                )
        except:
            return HttpResponse("401 Unauthorized", status=401)

        return function(request, id)


    return wrapper