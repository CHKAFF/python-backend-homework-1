import pytest

from app.internal.bank_cards.db.models import BankCard
from app.internal.services.bank_card_service import BankCardService

@pytest.mark.django_db
def test_get_by_telegram_user(test_telegram_user, test_bank_card):
    actual = BankCardService.get(test_telegram_user, test_bank_card.number)
    assert actual == test_bank_card

@pytest.mark.django_db
def test_get_by_number(test_bank_card):
    actual = BankCardService.get_by_number(test_bank_card.number)
    assert actual == test_bank_card

@pytest.mark.django_db
def test_get_by_number(test_bank_card, test_other_bank_card):
    amount = 1000
    old_amount = int(test_bank_card.bank_account_id.balance.amount)
    old_other_amount = int(test_other_bank_card.bank_account_id.balance.amount)
    actual = BankCardService.try_transfer_money(test_bank_card, test_other_bank_card, amount)
    assert actual == True
    actual = BankCardService.get_by_number(test_bank_card.number)
    assert int(actual.bank_account_id.balance.amount) == old_amount - amount
    actual = BankCardService.get_by_number(test_other_bank_card.number)
    assert int(actual.bank_account_id.balance.amount) == old_other_amount + amount