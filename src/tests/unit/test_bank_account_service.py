import pytest

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.services.bank_account_service import BankAccountService

@pytest.mark.django_db
def test_get_by_telegram_user(test_telegram_user, test_bank_account):
    actual = BankAccountService.get(test_telegram_user, test_bank_account.number)
    assert actual == test_bank_account

@pytest.mark.django_db
def test_get_all_by_telegram_user(test_telegram_user, test_bank_account):
    actual = BankAccountService.get_all(test_telegram_user)
    assert len(actual) == 1
    assert test_bank_account in actual

@pytest.mark.django_db
def test_get_by_number(test_bank_account):
    actual = BankAccountService.get_by_number(test_bank_account.number)
    assert actual == test_bank_account
    
@pytest.mark.django_db
def test_get_by_number(test_bank_account, test_other_bank_account):
    amount = 1000
    old_amount = int(test_bank_account.balance.amount)
    old_other_amount = int(test_other_bank_account.balance.amount)
    actual = BankAccountService.try_transfer_money(test_bank_account, test_other_bank_account, amount)
    assert actual == True
    actual = BankAccountService.get_by_number(test_bank_account.number)
    assert int(actual.balance.amount) == old_amount - amount
    actual = BankAccountService.get_by_number(test_other_bank_account.number)
    assert int(actual.balance.amount) == old_other_amount + amount