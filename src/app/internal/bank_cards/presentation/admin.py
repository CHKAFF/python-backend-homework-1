from django.contrib import admin

from app.internal.bank_cards.db.models import BankCard

@admin.register(BankCard)
class BankCardAdmin(admin.ModelAdmin):
    list_display = ('id','number' , 'bank_account_id')