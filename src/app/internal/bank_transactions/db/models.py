from django.db import models

from app.internal.bank_accounts.db.models import BankAccount

class BankTransaction(models.Model):
    sender = models.ForeignKey(
        BankAccount, 
        related_name="sender", 
        on_delete=models.CASCADE)
    
    recipient = models.ForeignKey(
        BankAccount, 
        related_name="recipient", 
        on_delete=models.CASCADE)

    amount = models.DecimalField(max_digits=14, decimal_places=2)

    def __str__(self):
        return (
            f"Sender: {self.sender.number}\n"
            f"Recipient: {self.recipient.number}\n"
            f"Amount: {self.amount}\n"
        )
