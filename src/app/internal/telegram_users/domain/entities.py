from typing import Optional

from ninja.orm import create_schema

from ..db.models import TelegramUser

TelegramUserSchemaBase = create_schema(TelegramUser, exclude=["password", "phone_number"])


class TelegramUserSchema(TelegramUserSchemaBase):
    phone_number: Optional[str]

    @staticmethod
    def resolve_phone_number(obj):
        if not obj.phone_number:
            return
        return f"{obj.phone_number}"
