from ninja import NinjaAPI


from .core.jwt_auth import JWTAuth
from .core.exception_handlers import add_exception_handlers
from .telegram_users.api import add_telegram_users_api
from .tokens.api import add_auth_api


def get_api():
    api = NinjaAPI(
        title="PYTHON.COURSE.API",
        version="0.0.1",
        auth=[JWTAuth()],
    )

    add_auth_api(api=api)

    add_telegram_users_api(api=api)

    add_exception_handlers(api=api)

    return api
