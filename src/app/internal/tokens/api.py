from ninja import NinjaAPI

from .db.repositories import TokenRepository
from .domain.services import TokenService
from .presentation.handlers import TokenHandlers
from .presentation.routers import add_auth_router
from ..telegram_users.db.repositories import TelegramUserRepository


def add_auth_api(api: NinjaAPI):
    auth_handler = TokenHandlers(TokenService(TokenRepository(), TelegramUserRepository()))
    add_auth_router(api, auth_handler)
