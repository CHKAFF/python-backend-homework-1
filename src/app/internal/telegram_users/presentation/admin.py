from django.contrib import admin

from app.internal.telegram_users.db.models import TelegramUser

@admin.register(TelegramUser)
class TelegremUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'external_id', 'name' ,'phone_number', "password")