import pytest

from app.internal.services.phone_number_service import PhoneNumberService

def test_correct_phone_number():
    actual = PhoneNumberService.is_correct("+79324971778")
    assert actual == True
    
def test_not_correct_phone_number():
    actual = PhoneNumberService.is_correct("+987000000000d")
    assert actual == False