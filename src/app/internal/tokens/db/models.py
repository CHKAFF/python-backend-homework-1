import uuid

from django.db import models
from django.utils import timezone

from app.internal.telegram_users.db.models import TelegramUser


class Token(models.Model):
    jti = models.CharField(max_length=255, primary_key=True)
    user = models.ForeignKey(
        TelegramUser, 
        on_delete=models.CASCADE, 
        related_name="refresh_tokens"
    )
    created_at = models.DateTimeField(default=timezone.now)
    revoked = models.BooleanField(default=False)
