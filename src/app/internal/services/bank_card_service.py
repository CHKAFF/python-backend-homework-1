import telegram
from ..telegram_users.db.models import TelegramUser
from ..bank_cards.db.models import BankCard
from django.db import transaction

from ..services.bank_account_service import BankAccountService

class BankCardService:
    def get(telegram_user, number):
        return BankCard.objects.filter(owner_id=telegram_user.id, number=number).first()
    
    def get_by_number(number):
        return BankCard.objects.filter(number=number).first()

    @transaction.atomic
    def try_transfer_money(card, recipient_card, amount):
        try:
            return BankAccountService.try_transfer_money(card.bank_account_id, recipient_card.bank_account_id, amount)
        except BankCard.DoesNotExist:
            return False
