from django.db import models
from django.core.validators import RegexValidator

class TelegramUser(models.Model):
    external_id = models.PositiveIntegerField(
        verbose_name="Telegram user chat id",
        null=False,
        blank=False,
        unique=True
    )
    name = models.TextField(
        verbose_name="Telegram user name",
        null=False,
        blank=False
    )
    phone_regex = RegexValidator(
        regex=r'^(\+)?((\d{2,3}) ?\d|\d)(([ -]?\d)|( ?(\d{2,3}) ?)){5,12}\d$', 
        message="Phone number is not correct."
    )
    phone_number = models.CharField(
        verbose_name="Telegram user phone number",
        validators=[phone_regex], 
        max_length=17,
        null=False,
        blank=True
    )
    favorite_users = models.ManyToManyField("self", 
        verbose_name="Favorite users list",
        symmetrical=False, 
        blank=True
    )

    password = models.BinaryField( #password_hash
        verbose_name="Password hash",
        editable=True, 
        null=True, 
        max_length=70000)

    def __str__(self):
        return f"Id: {self.external_id}, Name: {self.name}, Phone number: {self.phone_number}"
