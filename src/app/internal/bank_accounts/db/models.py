from django.db import models
from django.core.validators import RegexValidator
from djmoney.models.fields import MoneyField

from app.internal.telegram_users.db.models import TelegramUser

class BankAccount(models.Model):
    number_validator = RegexValidator(
        regex="\d{20}",
        message="Bank account number is not correct")

    number = models.CharField(
        max_length=20,
        validators=[number_validator],
        verbose_name="Bank account number",
        null=False,
        blank=False
    )
    owner_id = models.ForeignKey(
        TelegramUser, 
        verbose_name="Owner id",
        null=False,
        blank=False,
        on_delete=models.CASCADE
    )
    name = models.TextField(
        verbose_name="Bank account name",
        null=False,
        blank=False
    )
    balance = MoneyField(
        verbose_name="Bank account balance",
        null=False,
        blank=False,
        max_digits=14,
        default_currency='RUB'
    )

    def __str__(self):
        return f"Name: {self.name}, Number: {self.number}, Balance: {self.balance}"