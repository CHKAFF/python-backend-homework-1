from django.contrib import admin

from app.internal.bank_transactions.db.models import BankTransaction


@admin.register(BankTransaction)
class BankTransactionAdmin(admin.ModelAdmin):
    fields = ["sender", "recipient", "amount"]
