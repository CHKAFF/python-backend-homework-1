from ninja import NinjaAPI

from .db.repositories import TelegramUserRepository
from .domain.services import TelegramUserService
from .presentation.handlers import TelegramUserHandlers
from .presentation.routers import get_telegram_user_router


def add_telegram_users_api(api: NinjaAPI):
    handler = TelegramUserHandlers(person_service=TelegramUserService(person_repo=TelegramUserRepository()))
    get_telegram_user_router(api, handler)
