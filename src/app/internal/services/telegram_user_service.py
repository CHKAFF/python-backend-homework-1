from argparse import ArgumentError
from ..telegram_users.db.models import TelegramUser
from ..bank_transactions.db.models import BankTransaction
from django.db import transaction
from django.db.models import Q

from ..services.bank_account_service import BankAccountService

class TelegramUserService:
    def get(self, external_id = None, name = None):
        return TelegramUser.objects.filter(Q(external_id=external_id) | Q(name=name)).first()
    
    def create(self, external_id, name):
        if self.is_user_exist(external_id):
            raise ArgumentError(message=f"Telegram user with external_id = {external_id} is exist.")
        user = TelegramUser.objects.create(
            external_id = external_id,
            name = name
        )
        return user

    def update(self, external_id, name = None, phone_number = None, password = None):
        found_user = self.get(external_id=external_id)
        if found_user == None:
            raise ArgumentError(message = f"Telegram user with external_id = {external_id} is not exist.")
        
        if (name != None):
            found_user.name = name
        if (phone_number != None):
            found_user.phone_number = phone_number
        if (password != None):
            found_user.password = password
        found_user.save()

    def is_user_exist(self, external_id = None, name = None):
        return self.get(external_id=external_id, name=name) != None
        
    def get_favorite_users(self, external_id):
        return self.get(external_id = external_id).favorite_users.all()

    def try_add_user_to_favorites(self, external_id, name):
        user = self.get(external_id)
        user_to_add = self.get(name=name)
        if (user == None or user_to_add == None):
            return False
        user.favorite_users.add(user_to_add)
        user.save()
        return True

    def try_remove_user_from_favorites(self, external_id, name):
        user = self.get(external_id)
        user_to_remove = self.get(name=name)
        if (user == None or user_to_remove == None):
            return False
        if (user_to_remove not in user.favorite_users.all()):
            return False
        user.favorite_users.remove(user_to_remove)
        user.save()
        return True

    @transaction.atomic
    def try_transfer_money(self, external_id, name, amount):
        user = self.get(external_id)
        user_to_transfer_money = self.get(name=name)
        if (user == None or user_to_transfer_money == None):
            return False, "User not found"
        user_bank_accounts = BankAccountService.get_all(user)
        user_to_transfer_bank_accounts = BankAccountService.get_all(user_to_transfer_money)
        if (len(user_bank_accounts) == 0 or len(user_to_transfer_bank_accounts) == 0):
            return False, "User have not bank account"
        user_account = user_bank_accounts[0]
        user_to_transfer_account = user_to_transfer_bank_accounts[0]

        return BankAccountService.try_transfer_money(user_account, user_to_transfer_account, amount), ""



