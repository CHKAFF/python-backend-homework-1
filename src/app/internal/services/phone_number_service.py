import re

from django.conf import settings

class PhoneNumberService:
    def is_correct(phone_number):
        return re.fullmatch(settings.PHONE_NUMBER_REGEX_PATTERN, phone_number) is not None