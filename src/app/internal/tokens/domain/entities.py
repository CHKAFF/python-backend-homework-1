from ninja import Schema
from pydantic import Field


class PersonCredentialsSchema(Schema):
    telegram_username: str = Field(max_length=255)
    password: str = Field(max_length=512)


class JWTTokensSchema(Schema):
    access_token: str = Field(max_length=512)
    expires_at: float
    refresh_token: str = Field(max_length=512)


class JWTRefreshTokenSchema(Schema):
    refresh_token: str = Field(max_length=512)
