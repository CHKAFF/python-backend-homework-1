from django.http import HttpRequest
from ninja import Body

from ..domain.entities import JWTRefreshTokenSchema, JWTTokensSchema, PersonCredentialsSchema
from ..domain.services import TokenService
from app.internal.core.exceptions import ForbiddenException, UnauthorizedException
from ...services.user_password_service import UserPasswordService


class TokenHandlers:
    def __init__(self, token_service: TokenService):
        self._token_service = token_service

    def login(self, request: HttpRequest, person_credentials: PersonCredentialsSchema = Body(...)):
        telegram_user = self._token_service.get_person_by_telegram_username(
            telegram_username=person_credentials.telegram_username
        )
        if not telegram_user:
            raise UnauthorizedException("Person does not exists")

        if not UserPasswordService.encrypt(person_credentials.password) == bytes(telegram_user.password_hash):
            raise ForbiddenException("Invalid password")

        return self._token_service.generate_tokens(telegram_user)
