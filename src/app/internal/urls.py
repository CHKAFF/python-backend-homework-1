from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from .transport.rest import handlers
from .transport.rest.login_view import LoginView
from .transport.rest.refresh_view import RefreshView

urlpatterns = [
    path('me/<int:id>', handlers.me),
    path("auth/login", csrf_exempt(LoginView.as_view()), name="login"),
    path("auth/refresh", csrf_exempt(RefreshView.as_view()), name="refresh")
]
