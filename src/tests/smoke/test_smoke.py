import pytest
from django.test import Client, RequestFactory

@pytest.mark.django_db
def test_me_view(client, test_telegram_user):
    response = client.get("/api/me/"+f"{test_telegram_user.external_id}")
    assert response.status_code == 200
