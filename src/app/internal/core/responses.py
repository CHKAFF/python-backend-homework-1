from ninja import Schema


class SuccessResponse(Schema):
    success: object

class ErrorResponse(Schema):
    message: str

class ForbiddenResponse(ErrorResponse):
    ...

class UnauthorizedResponse(ErrorResponse):
    ...
