import json
from datetime import datetime, timedelta

from django.http import HttpRequest, HttpResponse, JsonResponse
from django.views import View

from ....internal.services.jwt_service import JWTService
from ....internal.services.user_password_service import UserPasswordService
from ....internal.services.telegram_user_service import TelegramUserService



class RefreshView(View):
    def post(self, request: HttpRequest):
        content = json.loads(request.body)
        refresh_token = content["refresh_token"]
        payload = JWTService.decode_token(refresh_token)
        token = JWTService.get_refresh_token_by_jti(payload["jti"])
        if not token:
            return HttpResponse("401 Unauthorized", status=401)
        if (token.created_at + timedelta(days=30)).timestamp() < datetime.utcnow().timestamp():
            return HttpResponse("401 Unauthorized. Refresh token expired", status=401)
        user = token.user
        access_token, expires_at = JWTService.create_access_token(user_id=user.external_id)
        JWTService.remove_refresh_token_by_person(user=token.user)
        refresh_token = JWTService.create_refresh_token(user=user)
        return JsonResponse({"access_token": access_token, "expires_at": expires_at, "refresh_token": refresh_token})
        
