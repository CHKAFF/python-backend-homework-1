from django.contrib import admin

from app.internal.bank_accounts.db.models import BankAccount

@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ('id','number' , 'owner_id', 'name', "balance")