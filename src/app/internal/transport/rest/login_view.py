import json

from django.http import HttpRequest, HttpResponse, HttpResponseForbidden, JsonResponse
from django.views import View

from ....internal.services.jwt_service import JWTService
from ....internal.services.user_password_service import UserPasswordService
from ....internal.services.telegram_user_service import TelegramUserService

class LoginView(View):
    def post(self, request: HttpRequest):
        content = json.loads(request.body)
        login = content["login"]
        telegramUserService = TelegramUserService()
        user = telegramUserService.get(name=login)
        if not user:
            return HttpResponse("401 Unauthorized", status=401)
        password = content["password"]
        password_hash = UserPasswordService.encrypt(password=password)
        expected_password = bytes(user.password)
        if expected_password != password_hash:
            return HttpResponse("401 Unauthorized", status=401)
        access_token, expires_at = JWTService.create_access_token(user.external_id)
        refresh_token = JWTService.create_refresh_token(user)
        response_content = {"access_token": access_token, "expires_at": expires_at, "refresh_token": refresh_token}
        return JsonResponse(response_content)
