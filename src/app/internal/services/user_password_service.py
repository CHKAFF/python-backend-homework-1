import hashlib

from django.conf import settings

class UserPasswordService:
    def encrypt(password):
        salt = settings.SALT.encode("utf-8")
        password = password.encode("utf-8")
        return hashlib.pbkdf2_hmac("sha256", password, salt, 100000)
