from ninja import NinjaAPI, Router

from app.internal.core.responses import SuccessResponse, UnauthorizedResponse
from ..domain.entities import TelegramUserSchema


def get_telegram_user_router(handlers) -> Router:
    router = Router(tags=["telegram_users"])

    router.add_api_operation(
        "/me/",
        ["GET"],
        handlers.me,
        response={200: TelegramUserSchema, 401: UnauthorizedResponse},
        url_name="telegram_users/me",
    )

    router.add_api_operation(
        "/me/password",
        ["PUT"],
        handlers.set_my_password,
        response={200: SuccessResponse, 401: UnauthorizedResponse},
        url_name="telegram_users/set_my_password",
    )

    return router


def add_telegram_users_router(api: NinjaAPI, handlers):
    router = get_telegram_user_router(handlers)
    api.add_router("/telegram_users", router)
