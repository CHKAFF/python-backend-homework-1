from ...services.user_password_service import UserPasswordService
from ..db.repositories import ITelegramUserRepository


class TelegramUserService:
    def __init__(self, repo: ITelegramUserRepository):
        self._repo = repo

    def get_telegram_user_by_external_id(self, external_id):
        return self._repo.get_telegram_user_by_external_id(external_id=external_id)

    def set_password_to_telegram_user(self, external_id, password):
        password_hash = UserPasswordService.encrypt(password)
        return self._repo.set_password_hash_to_telegram_user(external_id=external_id, password_hash=password_hash)
