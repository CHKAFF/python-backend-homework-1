from django.http import HttpRequest
from ninja import Body

from app.internal.core.responses import SuccessResponse


class TelegramUserHandlers:
    def __init__(self, service):
        self._service = service

    def me(self, request: HttpRequest):
        external_id = request.auth
        return self._service.get_telegram_user_by_external_id(external_id=external_id)

    def set_my_password(self, request: HttpRequest, password: str = Body(...)):
        external_id = request.auth
        self._service.set_password_to_telegram_user(external_id=external_id, password=password)
        return SuccessResponse()
