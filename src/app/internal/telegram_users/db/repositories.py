from app.internal.telegram_users.db.models import TelegramUser


class ITelegramUserRepository:
    def get_telegram_user_by_telegram_username(self, name):
        ...

    def get_telegram_user_by_external_id(self, external_id):
        ...

    def set_password_hash_to_telegram_user(self, external_id, password_hash):
        ...


class TelegramUserRepository(ITelegramUserRepository):
    def get_telegram_user_by_telegram_username(self, name):
        return TelegramUser.objects.filter(name=name).first()

    def get_telegram_user_by_external_id(self, external_id):
        return TelegramUser.objects.filter(external_id=external_id).first()

    def set_password_hash_to_telegram_user(self, external_id, password_hash):
        TelegramUser.objects.filter(external_id=external_id).update(password_hash=password_hash)
