import pytest

from app.internal.telegram_users.db.models import TelegramUser
from app.internal.services.telegram_user_service import TelegramUserService

@pytest.mark.django_db
def test_get_by_external_id(test_telegram_user):
    tg_service = TelegramUserService()
    actual = tg_service.get(external_id=test_telegram_user.external_id)
    assert actual == test_telegram_user

@pytest.mark.django_db
def test_get_by_name(test_telegram_user):
    tg_service = TelegramUserService()
    actual = tg_service.get(name=test_telegram_user.name)
    assert actual == test_telegram_user

@pytest.mark.django_db
def test_update(test_telegram_user):
    new_name = "new name"
    tg_service = TelegramUserService()
    actual = tg_service.get(name=test_telegram_user.name)
    assert actual == test_telegram_user
    assert new_name != test_telegram_user.name
    test_telegram_user.name = new_name
    tg_service.update(test_telegram_user.external_id, test_telegram_user.name)
    actual = tg_service.get(test_telegram_user.external_id)
    assert actual == test_telegram_user
    assert actual.name == new_name


@pytest.mark.django_db
def test_get_favorite_users(test_telegram_user, test_other_telegram_user):
    test_telegram_user.favorite_users.add(test_other_telegram_user)
    test_telegram_user.save()
    tg_service = TelegramUserService()
    actual = tg_service.get_favorite_users(test_telegram_user.external_id)
    assert test_other_telegram_user in actual

@pytest.mark.django_db
def test_add_favorite_users(test_telegram_user, test_other_telegram_user):
    tg_service = TelegramUserService()
    actual = tg_service.get_favorite_users(test_telegram_user.external_id)
    assert len(actual) == 0
    actual = tg_service.try_add_user_to_favorites(test_telegram_user.external_id, test_other_telegram_user.name)
    assert actual == True
    actual = tg_service.try_add_user_to_favorites(test_telegram_user.external_id, "Рандомное не существующее имя")
    assert actual == False
    actual = tg_service.get_favorite_users(test_telegram_user.external_id)
    assert len(actual) == 1
    assert test_other_telegram_user in actual

@pytest.mark.django_db
def test_remove_favorite_users(test_telegram_user, test_other_telegram_user):
    tg_service = TelegramUserService()
    actual = tg_service.get_favorite_users(test_telegram_user.external_id)
    assert len(actual) == 0
    tg_service.try_add_user_to_favorites(test_telegram_user.external_id, test_other_telegram_user.name)
    actual = tg_service.get_favorite_users(test_telegram_user.external_id)
    assert len(actual) == 1
    actual = tg_service.try_remove_user_from_favorites(test_telegram_user.external_id, test_other_telegram_user.name)
    assert actual == True
    actual = tg_service.try_remove_user_from_favorites(test_telegram_user.external_id, "Рандомное не существующее имя")
    assert actual == False
    actual = tg_service.get_favorite_users(test_telegram_user.external_id)
    assert len(actual) == 0