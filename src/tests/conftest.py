import pytest
from djmoney.money import Money

from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_card import BankCard
from app.internal.models.telegram_user import TelegramUser


@pytest.fixture(scope="function")
def test_telegram_user(external_id=0, name="name", phone_number="+79224758932"):
    return TelegramUser.objects.create(
        external_id=external_id,
        name=name,
        phone_number=phone_number
    )

@pytest.fixture(scope="function")
def test_other_telegram_user(external_id=1, name="name1", phone_number="+79224758954"):
    return TelegramUser.objects.create(
        external_id=external_id,
        name=name,
        phone_number=phone_number
    )

@pytest.fixture(scope="function")
def test_bank_account(test_telegram_user, number="0" * 20, name="Sber", balance=100000):
    return BankAccount.objects.create(number=number, owner_id=test_telegram_user, name=name, balance=Money(balance, "RUB"))

@pytest.fixture(scope="function")
def test_other_bank_account(test_other_telegram_user, number="1" * 20, name="Sber", balance=100000):
    return BankAccount.objects.create(number=number, owner_id=test_other_telegram_user, name=name, balance=Money(balance, "RUB"))


@pytest.fixture(scope="function")
def test_bank_card(test_telegram_user, test_bank_account, number="0000-0000-0000-0000"):
    return BankCard.objects.create(number=number, owner_id=test_telegram_user, bank_account_id=test_bank_account)

@pytest.fixture(scope="function")
def test_other_bank_card(test_other_telegram_user, test_other_bank_account, number="1111-1111-1111-1111"):
    return BankCard.objects.create(number=number, owner_id=test_other_telegram_user, bank_account_id=test_other_bank_account)
