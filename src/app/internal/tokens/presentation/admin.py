from django.contrib import admin

from app.internal.tokens.db.models import Token


@admin.register(Token)
class IssuedTokenAdmin(admin.ModelAdmin):
    fields = ["jti", "user", "created_at", "revoked"]
