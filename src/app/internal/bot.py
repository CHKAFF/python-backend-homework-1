from telegram import Bot, Update
from telegram.ext import CommandHandler, Dispatcher

from django.conf import settings
from .transport.bot import handlers

class TelegramBot:
    def __init__(self, token):
        self.bot = Bot(token)
        self.dispatcher = Dispatcher(self.bot, None, workers=0)

        self.dispatcher.add_handler(CommandHandler('start', handlers.start))
        self.dispatcher.add_handler(CommandHandler('me', handlers.me))
        self.dispatcher.add_handler(CommandHandler('bank_account', handlers.bank_account))
        self.dispatcher.add_handler(CommandHandler('bank_card', handlers.bank_card))
        self.dispatcher.add_handler(CommandHandler('get_favorite_users', handlers.get_favorite_users))
        self.dispatcher.add_handler(CommandHandler('set_phone', handlers.set_phone))
        self.dispatcher.add_handler(CommandHandler('add_favorite_user', handlers.add_user_to_favorites))
        self.dispatcher.add_handler(CommandHandler('remove_favorite_user', handlers.remove_user_from_favorites))
        self.dispatcher.add_handler(CommandHandler('transfer_money', handlers.transfer_money))
        self.dispatcher.add_handler(CommandHandler('bank_account_transfer_money', handlers.transfer_money_by_bank_accounts))
        self.dispatcher.add_handler(CommandHandler('bank_card_transfer_money', handlers.transfer_money_by_bank_card))
        self.dispatcher.add_handler(CommandHandler('bank_transactions', handlers.get_transactions))
        self.dispatcher.add_handler(CommandHandler('interacted', handlers.get_interacted))
        self.dispatcher.add_handler(CommandHandler('set_password', handlers.set_password))
    
    def process(self, t_data):
        update = Update.de_json(t_data, self.bot)
        self.dispatcher.process_update(update)
