from datetime import datetime

from ninja.security import HttpBearer

from ..services.jwt_service import JWTService

class JWTAuth(HttpBearer):
    def authenticate(self, request, token):
        try:
            payload = JWTService.decode_token(token)
            expires_at = payload["expires_at"]
            if expires_at > datetime.utcnow().timestamp():
                return payload["telegram_id"]
        except:
            return None
