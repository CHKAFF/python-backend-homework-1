from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt
from app.internal.transport.rest.handlers import BotView
from django.urls import path

from app.internal.api import get_api

api = get_api()

urlpatterns = [
    path("admin/", admin.site.urls),
    path('api/', api.urls),
    path("webhooks", csrf_exempt(BotView.as_view())),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
