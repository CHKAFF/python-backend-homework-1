from django.contrib import admin

from app.internal.admin_users.presentation.admin import AdminUserAdmin
from app.internal.telegram_users.presentation.admin import TelegramUser
from app.internal.bank_accounts.presentation.admin import BankAccount
from app.internal.bank_cards.presentation.admin import BankCard
from app.internal.bank_transactions.presentation.admin import BankTransaction
from app.internal.tokens.presentation.admin import Token

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
