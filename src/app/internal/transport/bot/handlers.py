from telegram import Update, message
from telegram.ext import CallbackContext

from .decorators import phone_number_should_be_filled
from ....internal.services.telegram_user_service import TelegramUserService
from ....internal.services.phone_number_service import PhoneNumberService
from ....internal.services.bank_account_service import BankAccountService
from ....internal.services.bank_card_service import BankCardService
from ....internal.services.bank_transaction_service import BankTransactionService
from ....internal.services.user_password_service import UserPasswordService

def start(update: Update, context: CallbackContext):
    external_id = update.message.chat_id
    name = update.message.from_user.username
    tg_user_service = TelegramUserService()

    if tg_user_service.is_user_exist(external_id):
        message = "Bot is already started :)"
    else:
        user = tg_user_service.create(external_id, name)
        message = f"Hello, {name}! Add a phone number to get complete information about yourself. Use the /set_phone command to save the phone number."
    
    context.bot.send_message(chat_id=update.effective_chat.id, text=message)

def set_phone(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id

    if (len(context.args) == 0):
        message = f'Use pattern: /set_phone <your number>'
        context.bot.send_message(chat_id=chat_id, text=message)
        return
    
    phone_number = context.args[0]
    
    if (not PhoneNumberService.is_correct(phone_number)):
        message = f'Phone number {phone_number} is not correct'
        context.bot.send_message(chat_id=chat_id, text=message)
        return
    
    external_id = update.message.chat_id
    tg_user_service = TelegramUserService()

    tg_user_service.update(external_id, phone_number=phone_number)

    message = f'Phone number {phone_number} is setted. You can use the /me command to get all information about yourself.'
    context.bot.send_message(chat_id=chat_id, text=message)

@phone_number_should_be_filled
def me(update: Update, context: CallbackContext):
    external_id = update.message.chat_id
    tg_user_service = TelegramUserService()

    user = tg_user_service.get(external_id)
    if user == None:
        message = f"Information about you is lost"
    else:
        chat_id = update.effective_chat.id
        message = str(user)
    
    context.bot.send_message(chat_id=chat_id, text=message)

def bank_account(update: Update, context: CallbackContext):
    help_message = f'Use pattern: /bank_account <number> <command> <command params..>'
    chat_id = update.effective_chat.id
    args_count = len(context.args)
    if (args_count < 2):
        context.bot.send_message(chat_id=chat_id, text=help_message)
        return
    number = context.args[0]
    command = context.args[1]
    if (command == "balance"):
        external_id = update.message.chat_id
        tg_user_service = TelegramUserService()
        user = tg_user_service.get(external_id)
        if user == None:
            message = f"Information about you is lost"
        else:
            bank_account = BankAccountService.get(user, number)
            if (bank_account):
                message = f'Bank account number: {number}\nBalance: {bank_account.balance}'
            else:
                message = "Bank account is not found"
        context.bot.send_message(chat_id=chat_id, text=message)
    else:
        context.bot.send_message(chat_id=chat_id, text=help_message)

def bank_card(update: Update, context: CallbackContext):
    help_message = f'Use pattern: /bank_card <number> <command> <command params..>'
    chat_id = update.effective_chat.id
    args_count = len(context.args)
    if (args_count < 2):
        context.bot.send_message(chat_id=chat_id, text=help_message)
        return
    number = context.args[0]
    command = context.args[1]
    if (command == "balance"):
        external_id = update.message.chat_id
        tg_user_service = TelegramUserService()
        user = tg_user_service.get(external_id)
        if user == None:
            message = f"Information about you is lost"
        else:
            bank_account = BankCardService.get(user, number)
            if (bank_account):
                message = f'Bank card number: {number}\nBalance: {bank_account.balance}'
            else:
                message = "Bank card is not found"
        context.bot.send_message(chat_id=chat_id, text=message)
    else:
        context.bot.send_message(chat_id=chat_id, text=help_message)

def get_favorite_users(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    external_id = update.message.chat_id
    tg_user_service = TelegramUserService()
    user = tg_user_service.get(external_id)
    if (user == None):
        message = f"Information about you is lost"
    else:
        favorite_users = tg_user_service.get_favorite_users(external_id)
        message = "Favorite users:\n"
        if (len(favorite_users) == 0):
            message += "Empty =("
        for favorite_user in favorite_users:
            message += str(favorite_user)
            message += "\n"
    context.bot.send_message(chat_id=chat_id, text=message)

def add_user_to_favorites(update, context):
    chat_id = update.effective_chat.id
    external_id = update.message.chat_id
    tg_user_service = TelegramUserService()
    user = tg_user_service.get(external_id)
    if (user == None):
        message = f"Information about you is lost"
    else:
        if (len(context.args) != 1):
            message = "Use pattern: /add_favorite_user <username>"
        elif tg_user_service.try_add_user_to_favorites(external_id, context.args[0]):
            message = "Success! Check your favorite list /get_favorite_users"
        else:
            message = "Oops.. I not found this user in my database"
    context.bot.send_message(chat_id=chat_id, text=message)

def remove_user_from_favorites(update, context):
    chat_id = update.effective_chat.id
    external_id = update.message.chat_id
    tg_user_service = TelegramUserService()
    user = tg_user_service.get(external_id)
    if (user == None):
        message = f"Information about you is lost"
    else:
        if (len(context.args) != 1):
            message = "Use pattern: /add_favorite_user <username>"
        elif tg_user_service.try_remove_user_from_favorites(external_id, context.args[0]):
            message = "Success! Check your favorite list /get_favorite_users"
        else:
            message = "Oops.. I not found this user in my database or in your favorite list"
    context.bot.send_message(chat_id=chat_id, text=message)


def transfer_money(update, context):
    chat_id = update.effective_chat.id
    external_id = update.message.chat_id
    tg_user_service = TelegramUserService()
    user = tg_user_service.get(external_id)
    if (user == None):
        message = f"Information about you is lost"
    else:
        if (len(context.args)) == 2 and context.args[1].isdigit():
            amount = int(context.args[1])
            if amount <= 0:
                message = "Amount should be positive"
            else:
                is_success, error_message = tg_user_service.try_transfer_money(update.effective_user.id, context.args[0], amount) 
                if is_success:
                    message = "Success!"
                else:
                    message = f"Oops.. Operations is failed. {error_message}"
        else:
            message = "Use pattern: /transfer_money <username> <amount>"
    context.bot.send_message(chat_id=chat_id, text=message)

def transfer_money_by_bank_accounts(update, context):
    chat_id = update.effective_chat.id
    external_id = update.message.chat_id
    tg_user_service = TelegramUserService()
    user = tg_user_service.get(external_id)
    if (user == None):
        message = f"Information about you is lost"
    else:
        if (len(context.args)) == 3 and context.args[-1].isdigit():
            amount = int(context.args[-1])
            if amount <= 0:
                message = "Amount should be positive"
            else:
                bank_account_number = context.args[0]
                recipient_bank_account_number = context.args[1]
                bank_account = BankAccountService.get(user, bank_account_number)
                recipient_bank_account = BankAccountService.get_by_number(recipient_bank_account_number)
                if (bank_account == None):
                    message = "Oops.. I not found your bank account"
                elif (recipient_bank_account == None):
                    message = "Oops.. I not found recipient bank account"
                else:
                    if BankAccountService.try_transfer_money(bank_account, recipient_bank_account, amount):
                        message = "Success!"
                    else:
                        message = f"Oops.. Operations is failed."
        else:
            message = "Use pattern: /bank_account_transfer_money <your_bank_account> <recipient_bank_account> <amount>"
    context.bot.send_message(chat_id=chat_id, text=message)

def transfer_money_by_bank_card(update, context):
    chat_id = update.effective_chat.id
    external_id = update.message.chat_id
    tg_user_service = TelegramUserService()
    user = tg_user_service.get(external_id)
    if (user == None):
        message = f"Information about you is lost"
    else:
        if (len(context.args)) == 3 and context.args[-1].isdigit():
            amount = int(context.args[-1])
            if amount <= 0:
                message = "Amount should be positive"
            else:
                bank_card_number = context.args[0]
                recipient_bank_card_number = context.args[1]
                bank_card = BankCardService.get(user, bank_card_number)
                recipient_bank_card = BankCardService.get_by_number(recipient_bank_card_number)
                if (bank_card == None):
                    message = "Oops.. I not found your bank card"
                elif (recipient_bank_card == None):
                    message = "Oops.. I not found recipient bank card"
                else:
                    if BankCardService.try_transfer_money(bank_card, recipient_bank_card, amount):
                        message = "Success!"
                    else:
                        message = f"Oops.. Operations is failed."
        else:
            message = "Use pattern: /bank_card_transfer_money <your_bank_account> <recipient_bank_account> <amount>"
    context.bot.send_message(chat_id=chat_id, text=message)

def get_transactions(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    external_id = update.message.chat_id
    if not context.args or len(context.args) != 1:
        context.bot.send_message(chat_id=chat_id, text=f"Use pattern: /bank_transactions <your_bank_account>")
        return

    number = context.args[0]

    bank_account = BankAccountService.get_by_number(number)
    if bank_account is None or bank_account.owner_id.external_id != external_id:
        context.bot.send_message(chat_id=chat_id, text="Bank account number is not found ")
        return

    bank_transactions = BankTransactionService.get_by_number(number=number)

    if not bank_transactions:
       context.bot.send_message(chat_id=chat_id, text="This bank account have not transactions")
       return

    message = f"Bank account {number} transactions:\n\n"
    for index, bank_transaction in enumerate(bank_transactions):
        message += (
            f"- Sender: @{bank_transaction.sender.owner_id.name}. "
            f"BankAccount: {bank_transaction.sender.number}. "
            f"Recipient: @{bank_transaction.recipient.owner_id.name}. "
            f"BankAccount: {bank_transaction.recipient.number}. "
            f"Amount: {bank_transaction.amount} {bank_transaction.sender.balance.currency}.\n"
        )
    context.bot.send_message(chat_id=chat_id, text=message)


def get_interacted(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    external_id = update.message.chat_id
    tg_user_service = TelegramUserService()
    user = tg_user_service.get(external_id)
    if (user == None):
        context.bot.send_message(chat_id=chat_id, text=f"Information about you is lost")
        return
    interacted = BankTransactionService.get_interacted(name=update.effective_user.username)
    if not interacted:
        context.bot.send_message(chat_id=chat_id, text="You have not interacted users")
    message = "Interacted users:"
    for index, interacted_with_username in enumerate(interacted):
        message += f"- @{interacted_with_username}\n"
    context.bot.send_message(chat_id=update.effective_chat.id, text=message)

def set_password(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    external_id = update.message.chat_id
    tg_user_service = TelegramUserService()
    user = tg_user_service.get(external_id)
    if (user == None):
        context.bot.send_message(chat_id=chat_id, text=f"Information about you is lost")
        return
    
    if not context.args or len(context.args) > 1:
        context.bot.send_message(chat_id=chat_id, text=f"Use pattern: /set_password <new_password>")
        return
    
    password_hash = UserPasswordService.encrypt(context.args[0])
    tg_user_service.update(external_id, password=password_hash)
    context.bot.send_message(chat_id=chat_id, text="Success!")

