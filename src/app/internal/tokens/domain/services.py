import uuid
from datetime import datetime, timedelta

from ..db.models import Token
from ..db.repositories import ITokenRepository
from .entities import JWTTokensSchema
from ...services.jwt_service import JWTService
from ...telegram_users.db.repositories import ITelegramUserRepository

ACCESS_TOKEN_EXPIRES_RELATED_TO_NOW = timedelta(minutes=5)
REFRESH_TOKEN_EXPIRES_RELATED_TO_NOW = timedelta(days=30)
JTI_FIELD = "jti"


class TokenService:
    def __init__(self, token_repo: ITokenRepository, telegram_user_repo: ITelegramUserRepository):
        self._token_repo = token_repo
        self.telegram_user_repo = telegram_user_repo

    def get_person_by_telegram_username(self, telegram_username):
        return self.telegram_user_repo.get_telegram_user_by_telegram_username(name=telegram_username)

    def generate_tokens(self, telegram_user):
        expires_at = (datetime.utcnow() + ACCESS_TOKEN_EXPIRES_RELATED_TO_NOW).timestamp()
        access_token = JWTService.encode_payload({"external_id": telegram_user.external_id, "expires_at": expires_at})
        issued_token = self._token_repo.create_issued_token(telegram_user=telegram_user)
        refresh_token = JWTService.encode_payload({JTI_FIELD: str(issued_token.jti)})
        return JWTTokensSchema(access_token=access_token, expires_at=expires_at, refresh_token=refresh_token)

    def get_issued_token_by_jti(self, jti):
        return self._token_repo.get_issued_token_by_jti(jti=jti)

    def check_issued_token_expiration(self, issued_token):
        expired_at = issued_token.created_at + REFRESH_TOKEN_EXPIRES_RELATED_TO_NOW
        return expired_at.timestamp() < datetime.utcnow().timestamp()

    def decode_jti_from_refresh_token(self, refresh_token):
        payload = JWTService.decode_token(refresh_token)
        return payload[JTI_FIELD]
