from itertools import chain
from typing import List

from django.db.models import Q

from app.internal.bank_transactions.db.models import BankTransaction

class BankTransactionService:

    def create(sender, recipient, amount):
        BankTransaction.objects.create(sender=sender, recipient=recipient, amount=amount)

    def get_by_number(number):
        return (
            BankTransaction.objects.select_related()
            .filter(Q(sender__number=number) | Q(recipient__number=number))
            .distinct()
        )

    def get_interacted(name):
        names = (
            BankTransaction.objects.filter(Q(sender__owner_id__name=name) | Q(recipient__owner_id__name=name))
            .distinct()
            .values_list("sender__owner_id__name", "recipient__owner_id__name")
        )
        return list(set(chain.from_iterable(names)) - {name})

# Tests