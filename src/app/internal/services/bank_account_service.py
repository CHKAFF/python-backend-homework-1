import telegram
from ..telegram_users.db.models import TelegramUser
from ..bank_accounts.db.models import BankAccount
from ..bank_transactions.db.models import BankTransaction
from django.db import transaction

from ..services.bank_transaction_service import BankTransactionService


class BankAccountService:
    def get(telegram_user, number):
        return BankAccount.objects.filter(owner_id=telegram_user.id, number=number).first()

    def get_by_number(number):
        return BankAccount.objects.filter(number=number).first()
    
    def get_all(telegram_user):
        return BankAccount.objects.filter(owner_id=telegram_user.id).all()

    @transaction.atomic
    def try_transfer_money(bank_account, recipient_bank_account, amount):
        try:
            if bank_account.balance.amount < amount:
                return False
            bank_account.balance.amount -= amount
            recipient_bank_account.balance.amount += amount # F()
            bank_account.save()
            recipient_bank_account.save()
            BankTransactionService.create(sender=bank_account, recipient=recipient_bank_account, amount=amount)
            return True
        except BankAccount.DoesNotExist:
            return False
