from django.core.management.base import BaseCommand
from ...internal import bot


class Command(BaseCommand):
    def handle(self, *args, **options):
        bot.run()
