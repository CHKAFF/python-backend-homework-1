from django.db import models
from django.core.validators import RegexValidator
from djmoney.models.fields import MoneyField

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.telegram_users.db.models import TelegramUser

class BankCard(models.Model):
    number_validator = RegexValidator(
        regex="\d{4}-\d{4}-\d{4}-\d{4}",
        message="Bank card number is not correct")

    number = models.CharField(
        max_length=19,
        validators=[number_validator],
        verbose_name="Bank card number",
        null=False,
        blank=False
    )
    owner_id = models.ForeignKey(
        TelegramUser, 
        verbose_name="Owner id",
        null=False,
        blank=False,
        on_delete=models.CASCADE
    )
    bank_account_id = models.ForeignKey(
        BankAccount, 
        verbose_name="Bank account id",
        null=False,
        blank=False,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f"Number: {self.number}, Balance: {self.bank_account_id.balance}, Bank account: {self.bank_account_id}"
